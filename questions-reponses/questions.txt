
--------------------------------------------------------
-- 420-523 -- Applications Multi-Tier -- Examen INTRA --
--------------------------------------------------------


--------
-- Notes

- Utilisation d'internet permise.
- Toute documentation permise.
- Communication interdite.

- Toutes les réponses doivent être de votre composition.
- Tout plagiat entrainera la perte totale des points de la question.
- Tout copier-coller venant d'internet sera considéré comme du plagiat.


------------
-- QUESTIONS


A -- Culture Générale (20 points)

Répondez aux questions en quelques phrases, par un énoncé clair et précis qui capture l'essentiel du/des concept(s) devant être expliqués.

1. Gestion de versions (6 pts)

Un étudiant de première année vous dit qu'il est stupide d'utiliser système de gestion de versions (ex. Git), surtout lorsque l'équipe est de taille minimale (ex. Une personne) ? Écrivez ce que vous lui répondez et justifiez votre réponse en donnant des exemples d'avantages et/ou d'inconvénients.

2. Cycles (7 pts)

Vous arrivez dans une nouvelle équipe. Votre patron ne connait rien en gestion et vous demande des conseils sur les cycles de développement. Il a vaguement entendu parler de "waterfall", "V", "sprint", "scrum" et "paintball". Expliquez-lui ce que vous savez sur les cycles de développement de manière à l'aider.

3. Agile (7 pts)

Vous rencontrez un vieil oncle éloigné à l'occasion de la fête de ses 80 ans. À table, ce dernier se montre très intéressé par les nouvelles technologies (vous êtes ami avec lui sur facebook) et vous pose beaucoup de questions. De retour à la maison, vous lui envoyez un e-mail dans lequel vous lui expliquez la méthodologie Agile. Écrivez ce email.


--


B -- Gestion de Projets (30 points)

On vous donne 200 000 $ et 3 mois pour développer une application multi-tier similaire à Stootie (genre de kijiji pour rendre service, voir https://stootie.com/), mais évidemment beaucoup mieux. Cela vous semble tout à fait réalisable. Fort de votre expérience, vous acceptez le projet.

Répondez aux questions suivantes en quelques phrases (pas besoin de donner trop de détails).

1. Décrivez votre architecture. (5 pts)

2. Quelles technologies allez-vous utiliser ? (5 pts)

3. De combien de personnes avez-vous besoin ? (5 pts)

4. Quels seront les rôles des personnes. (5 pts)

5. Comment allez-vous gérer le projet ? (5 pts)

6. Quelles sont, selon vous, les grandes étapes du projet ? (5 pts)


--


C -- Applications Multi-Tier (50 points)

En utilisant les technologies de votre choix, développez une application 3-tier minimale.

- Assurez-vous de placer tout le code dans le répertoire application.
- Si vous manquez de temps ou que vous ne savez pas comment développer une partie, simulez-là (mocking). 
- Écrivez les pré-requis et/ou directives nécessaires pour compiler et faire fonctionner votre application.

Votre application doit être composée de :

1. Une base de données ayant une table contenant les champs suivants : ID, nom, e-mail. Créez 3 entrées. Donnez les commandes nécessaires pour la création de la base de données. (10 pts)

2. Une couche application faisant la liaison entre la base de données et la couche présentation. (25 pts)

3. Une couche présentation dans laquelle l'usager entre un ID et obtient en retour le nom et l'adresse email correspondant. (15 pts)

