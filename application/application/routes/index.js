var express = require('express');
var router = express.Router();
var mongodb = require('mongodb');
var mongoose = require('mongoose');


var User = mongoose.model('User', {nom: String, email: String, ID: Number})
/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

function userfind (req, res, next) {
    mongoose.connect('mongodb://localhost:27017/intra3tiers');

    var id = req.query.id;
    User.findOne({ID: id}, function (err, userObj) {
        if (err) {
            console.log(err);
        } else if (userObj) {
            console.log('Found:', userObj);

            req.dataProcessed = userObj;
            return next();

        } else {
            console.log('User not found!');
            res.render('error', {
                error: {
                    status: "User not found"
                }
            });
        }
    });

    mongoose.connection.close()

}
router.get('/user', userfind, function(req, res, next) {
    var context = req.dataProcessed;
    res.render('user', {
        user: context
    });
});

module.exports = router;
